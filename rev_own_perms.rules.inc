<?php

/**
 * @file
 * Rules integration for Revisioning Own Permissions module.
 *
 * @addtogroup rules
 * @{
 */

/************************** Rules Conditions **************************************/

/**
 * Implements hook_rules_condition_info().
 */
function rev_own_perms_rules_condition_info() {
  $items['rev_own_perms_node_has_own'] = array(
    'group' => t('Revisioning'),
    'label' => t('Content has own revisions for user'),
    'help' => t('Evaluates to TRUE, if the content has an own revisions for the user.'),
    'parameter' => array(
      'node' => array('type' => 'node', 'label' => t('Content')),
      'user' => array('type' => 'user', 'label' => t('User')),
    ),
  );
  return $items;
}

/**
 * Condition: check for own revision of the node.
 */
function rev_own_perms_node_has_own($node, $user) {
  $own_revs = _rev_own_perms_node_revision($node->nid, $user->uid);
  return !empty($own_revs);
}

/************************** Rules Actions **************************************/

/**
 * Implements hook_rules_action_info().
 */
function rev_own_perms_rules_action_info() {
  return array(
    'rev_own_perms_rules_action_load_own' => array(
      'group' => t('Revisioning'),
      'label' => t('Load user\'s latest own revision of content'),
      'parameter' => array(
        'node' => array('type' => 'node', 'label' => t('content')),
        'user' => array('type' => 'user', 'label' => t('user')),
      ),
      'provides' => array(
        'loaded_own_latest_revision' => array(
          'type' => 'node',
          'label' => t('User\'s own loaded latest revision of content'),
          'save' => FALSE,
        ),
      ),
    ),
  );
}

/**
 * Action: load user's own latest revision of provided node.
 */
function rev_own_perms_rules_action_load_own($node, $user) {
  $own_vids = _rev_own_perms_node_revision($node, $user->uid);
  if ($own_vids) {
    $own_vid = array_shift($own_vids);
    if ($own_vid == $node->vid) {
      $own_node = $node;
    }
    else {
      $own_node = node_load($node->nid, $own_vid);
    }
  }
  else {
    $own_node = NULL;
  }
  return array('loaded_own_latest_revision' => $own_node);
}
