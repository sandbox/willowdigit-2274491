
DESCRIPTION
===========
This module is an add on module for revisioning, with its sole purpose
a set of permissions which will allow for viewing and editing one's own
revisions.

INSTALLATION & CONFIGURATION
============================
Note: For this module to work you MUST enable the radio button
'Every time [content type] content is updated, even when saving content in draft/pending moderation'
which is a configuration option installed by the Revisioning module under publishing options of the
the Content Type you are enabling.

1. Install as usual.
2. Grant sufficient permissions under 'revisioning', and then limit those permissions under 'revisioning own permissions'.

Note: You cannot grant yourself more permissions through this module than Node and
Revisioning does, you can only limit those permissions further.

AUTHOR
=======
Juan Reynolds, Johannesburg, South Africa.