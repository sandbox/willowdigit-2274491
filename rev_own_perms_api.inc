<?php
/**
 * @file
 * API functions of Revisioning Own Permissions module
 *
 * Reusable functions that do the dirty work.
 */

/**
 * Function to get all revisions of a user given a specific node id
 * @param $nid
 * @return mixed
 */
function _rev_own_perms_node_revision($nid, $uid = NULL) {
  if ($uid == NULL) {
    global $user;
    $uid = $user->uid;
  }
  $result = &drupal_static(__FUNCTION__);
  if (!isset($result)) {
    $result = db_query("SELECT vid, vid FROM {node_revision} WHERE nid = :nid AND uid = :uid ORDER BY vid DESC",
      array(
        ':nid' => $nid,
        ':uid' => $uid,
      ))
      ->fetchAllKeyed();
  }
  return $result;
}